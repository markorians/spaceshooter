# spaceSHooter

## An arcade space shooter written in pure BASH scripting.

### Only 348 line (+43 line of comment) of pure useless code.

### Requirements :
* BASH >= 4.0
* ncurses

### Controls :
* Arrow Keys [← → ↑ ↓] move space ship. 
* 'Z' key for shoot.
* 9/0 keys to switch on/off debugging info

![Demo](https://bitbucket.org/markorians/spaceshooter/raw/992f6e4e634d849b878711f73f8f5da2aee97120/play.gif)