#!/bin/bash

#########################################
############## DEBUGGING ################
#########################################

# 0 = Normal, 1 = Debug
DEBUG=0;

#########################################
########## GLOBAL VARIABLES #############
#########################################

# Terminal Line Number
LINES=0;

# Terminal Column Number
COLUMNS=0;

# Score reached
SCORE=0;

# Position of My Sprite
MYPOS=();

# Global Counter that Spawn Enemy Ship
GLOBAL_SPAWN_COUNTER=0;

# Global Subtractor of Enemy Ship Spawn Counter
GLOBAL_SPAWN_DIFF=100

# Global Subtractor of Enemy Bullets Spawn Counter
GLOBAL_ENEMY_BULLET_DIFF=30;

# Array of Global Counters that Spawn Enemy Bullet
declare -A GLOBAL_ENEMY_BULLET_COUNTER;

# Two-dimensional Array Representing the Battlefield
declare -A BATTLE_FIELD;

#########################################
############## FUNCTIONS ################
#########################################

# Get Current Terminal Size
function getCoords(){ LINES=$(tput lines); COLUMNS=$(tput cols); }

# Go To Specific Position
function goToPos(){ printf "\033[$1;$2H"; }

# Go To Zero Position
function goToZero(){ printf "\033[0;0H"; }

# Get Input From Keyboard
function getInput(){ read -s -t 0.01 -n 1 key; printf "$key"; }

# Print String with Shell Color Value
function printColor(){ printf "\e[1;$1m$2\e[0m"; }

# Build Two-dimensional Associative Array Representing the Battlefield
function buildBattleField(){
	BATTLE_FIELD=();
	local x=$1; local y=$3;
	while [ $x -le $2 ]; do
		while [ $y -le $4 ]; do
			BATTLE_FIELD[$x,$y]=0;
			let y++;
		done
		let x++;
		y=$3;
	done
}

# Evaluate Action to Take for each Entities
function evalEntitiesAction(){
	local l=0; local c=0;
	local line=$1; local col=$2; local val=$3;

	# Put Enemy Ship in Random Position if Difference is Reached.
	if [ $[ $GLOBAL_SPAWN_COUNTER - $GLOBAL_SPAWN_DIFF ] -eq 0 ]; then
		l=$[ $RANDOM % $[ $LINES / 2 ] + 2 ];
		c=$[ $RANDOM % $[ $COLUMNS - 1 ] + 2 ];
		BATTLE_FIELD["$l,$c"]=3;
		GLOBAL_SPAWN_COUNTER=0;
		GLOBAL_ENEMY_BULLET_COUNTER["$l,$c"]=0;
	fi

	case $val in
		"1")
			if [ ${BATTLE_FIELD[$[ $line - 1 ],$col]} -gt 0 ]; then
				youAreDie "YOU ARE DIE !!!";
			fi
		;;
		"2")
			if [ ${BATTLE_FIELD[$[ $line - 1 ],$col]} -eq 3 ]; then
				SCORE=$[ $SCORE + 1 ];
			fi

			BATTLE_FIELD["$line,$col"]=-1;
			BATTLE_FIELD["$[ $line - 1 ],$col"]=2;
		;;
		"3")
			if [ ${GLOBAL_ENEMY_BULLET_COUNTER[$line,$col]} -eq $GLOBAL_ENEMY_BULLET_DIFF ]; then
				if [ ${BATTLE_FIELD["$line,$col"]} -eq 3 ]; then
					BATTLE_FIELD["$[ $line + 1 ],$col"]=4;
					GLOBAL_ENEMY_BULLET_COUNTER["$line,$col"]=0;
				fi
			fi
		;;
		"4")
			if [ ${BATTLE_FIELD["$[ $line - 1 ],$col"]} -ne -1 ]; then
				BATTLE_FIELD["$line,$col"]=-1;
				if [ $line -lt $[ $LINES - 2 ] ]; then
					BATTLE_FIELD["$[ $line + 1 ],$col"]=4;
				fi
			fi
		;;
	esac
}

# Check Actions of My Character.
function checkCharacterActions(){
	local line; local col;

	line=${MYPOS[0]}; col=${MYPOS[1]};

	case $1 in
		"A")
			# Move up
			if [ $line -gt 2 ]; then
				BATTLE_FIELD["$line,$col"]=-1;
				BATTLE_FIELD["$[ $line - 1 ],$col"]=1;
				let line--;
			fi
		;;
		"B")
			# Move Down
			if [ $[ $LINES - $line ] -gt 2 ]; then
				BATTLE_FIELD["$line,$col"]=-1;
				BATTLE_FIELD["$[ $line + 1 ],$col"]=1;
				let line++;
			fi
		;;
		"C")
			# Move right
			if [ $[ $COLUMNS - $col ] -gt 2 ]; then
				BATTLE_FIELD["$line,$col"]=-1;
				BATTLE_FIELD["$line,$[ $col + 1 ]"]=1;
				let col++;
			fi
		;;
		"D")
			# Move left
			if [ $col -gt 2 ]; then
				BATTLE_FIELD["$line,$col"]=-1;
				BATTLE_FIELD["$line,$[ $col - 1 ]"]=1;
				let col--;
			fi
		;;
		"z")
			# Shoot From My Sprite
			BATTLE_FIELD["$[ ${MYPOS[0]} - 1 ],${MYPOS[1]}"]=2;
		;;
		"q")
			# Quit Game
			exit;
		;;
	esac

	goToZero;

	local dbg="";

	dbg+="MYPOS ${MYPOS[*]} - ";
	dbg+="LINE $LINES COL $COLUMNS - ";
	dbg+="CNT $GLOBAL_SPAWN_COUNTER - ";
	dbg+="SCR $SCORE - ";
	dbg+="BTN _$1_";

	[ $DEBUG -gt 0 ] && printf "$dbg";

	MYPOS=( $line $col );
}

# Translate Integer Value Representing Sprite
function spriteTranslate(){
	case $1 in
		0)
			# Empty
			printColor 0 ' ';
		;;
		1)
			# My Sprite
			printColor 92 'A';
		;;
		2)
			# My Bullet
			printColor 91 '°';
		;;
		3)
			# Enemy Sprite
			printColor 36 'V';
		;;
		4)
			# Enemy Bullet
			printColor 93 '*';
		;;
		9)
			# Field Borders ...
			if [ -n "$2" ]; then
				# ... with Custom String
				printColor 42 "$2";
			else
				# ... with Empty Space
				printColor 42 ' ';
			fi
		;;
	esac
}

# Draw Borders
function drawBorders(){
	goToZero;
	spriteTranslate 9 "%$[COLUMNS]s";

	local cnt=1;

	while [ $cnt -lt $LINES ];
	do
		goToPos $cnt 0;
		spriteTranslate 9;
		goToPos $cnt $COLUMNS;
		spriteTranslate 9;
		let cnt++;
	done

	goToPos $LINES 0;
	spriteTranslate 9 "%$[COLUMNS]s";
}

# Draw Battlefield and Evaluate Action to Take for each Entities
function drawBattlefield(){
	EMPTY_LINES=(); NOT_EMPTY_LINES=();

  # Split line indexes if some line is / isn't empty.
	local line=2; local col=1;
	while [ $line -lt $LINES ]; do
		LINE_CONTENT=();
		while [ $col -le $COLUMNS ];
		do
			LINE_CONTENT+=(${BATTLE_FIELD[$line,$col]});
			let col++;
		done

		if [ "$(echo ${LINE_CONTENT[*]} | tr ' ' '\n' | uniq -u)" = '' ]; then
			EMPTY_LINES+=("$line");
		else
			NOT_EMPTY_LINES+=("$line");
		fi

		let line++;
		col=1;
	done

  # Print All Empty Lines
	for i in ${EMPTY_LINES[*]}; do
		goToPos $i 2;
		printColor 0 "%$[COLUMNS - 2]s";
	done

  # Print and Evaluate each Column for not Empty Lines
	for i in ${NOT_EMPTY_LINES[*]}; do
		local cnt=1;
		while [ $cnt -le $[ $COLUMNS - 2 ] ]; do
			if [ ${BATTLE_FIELD[$i,$cnt]} -gt 0 ]; then
				goToPos $i $cnt;
				spriteTranslate ${BATTLE_FIELD[$i,$cnt]};
				evalEntitiesAction $i $cnt ${BATTLE_FIELD[$i,$cnt]};
			else
				if [ ${BATTLE_FIELD[$i,$cnt]} -lt 0 ]; then
					goToPos $i $cnt;
					spriteTranslate 0;
					BATTLE_FIELD["$i,$cnt"]=0;
				fi
			fi
			let cnt++;
		done
	done

	unset EMPTY_LINES; unset NOT_EMPTY_LINES; unset LINE_CONTENT;
}

# Start Game and Setting Inital Values
function initGame(){
	# Read Coords
	getCoords;
	# Draw Borders
	drawBorders;
	# Build Battlefield Array Logic
	buildBattleField 1 $[ $LINES - 2 ] 1 $[ $COLUMNS - 2 ];
	# Set Starting Position
	MYPOS=( $[LINES - 3] $[ $COLUMNS / 2 ] );
	BATTLE_FIELD["${MYPOS[0]},${MYPOS[1]}"]=1;
  # Set Score at Zero.
	SCORE=0;
}

# Function Executed when Game is Finished
function youAreDie(){
	goToPos $[ $LINES / 2 ] $[ $COLUMNS / 2 - ${#1} / 2 ];
	printColor 41 "$1";
  sleep 2; exit;
}

# Function Executed when Quitting Game
function onExit(){
	# Turn on Cursor
	printf "\e[?12l\e[?25h";
	# Reinitialize Terminal Settings
	stty "$_STTY";
	# Clear Screen
	clear;
}

#########################################
############# MAIN CICLE ################
#########################################

# Init terminal
tput reset;
# Save tty config
_STTY=$(stty -g);
# Disable cursor blinking
printf "\e[?25l";
# Disable keyboard output
stty -echo -icanon;
# Add Function 'onExit'
trap onExit ERR EXIT
# Init Game Settings
initGame;

while true; do

  # Reload Game if Resolution is Changed
	[ $LINES -ne $(tput lines) -a $COLUMNS -ne $(tput cols) ] && initGame;

  # Get User Input
	key="$(getInput)";

  # Check Actions to Take
	checkCharacterActions $key;

  # Check if Enable/Disable Debug Info
	[ "$key" = "9" ] || [ "$key" = "0" ] && DEBUG=$key;

  # Draw Battlefield
	drawBattlefield;

  # Tick Global Spawn Counter
	GLOBAL_SPAWN_COUNTER=$[ $GLOBAL_SPAWN_COUNTER + 1 ];

  # Print Score if There isn't Debug Enable
	if [ $DEBUG -eq 0 ]; then
		scr="  Score : $SCORE";
		goToZero;
		printColor 42 "$scr";
		spriteTranslate 9 "%$[ $COLUMNS - ${#scr} ]s";
	fi

  # If Debug is Enabled Clear Bottom Border Line
  if [ $DEBUG -gt 0 ]; then
  	goToPos $LINES 1;
  	spriteTranslate 9 "%$[COLUMNS]s";
  fi

  # Increment All Bullet Counters
	for c in ${!GLOBAL_ENEMY_BULLET_COUNTER[*]}; do
		GLOBAL_ENEMY_BULLET_COUNTER["$c"]=$[ ${GLOBAL_ENEMY_BULLET_COUNTER["$c"]} + 1];
	done

  # Print Bullet Counters
  if [ $DEBUG -gt 0 ]; then
    goToPos $LINES 1;
    for c in ${!GLOBAL_ENEMY_BULLET_COUNTER[*]}; do
      if [ ${GLOBAL_ENEMY_BULLET_COUNTER[$c]} -lt $GLOBAL_ENEMY_BULLET_DIFF ]; then
        printf "${GLOBAL_ENEMY_BULLET_COUNTER[$c]} ";
      fi
    done
  fi

done
